namespace UploadApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Exception_Log : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exception_Log",
                c => new
                {
                    exID = c.Int(nullable: false, identity: true),
                    exErrName = c.String(nullable: false, maxLength: 100),
                    exErrMsg = c.String(nullable: true, maxLength: 250),
                    exRefData = c.String(nullable: true, maxLength:50),
                    exCreated = c.DateTime(),
                })
                .PrimaryKey(t => t.exID);
        }
        
        public override void Down()
        {
            DropTable("dbo.Exception_Log");
        }
    }
}
