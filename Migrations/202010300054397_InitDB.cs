namespace UploadApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        invID = c.Int(nullable: false, identity: true),
                        invCode = c.String(nullable: false, maxLength: 50),
                        invAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        invCurrencyCode = c.String(nullable: false, maxLength: 3),
                        invStatus = c.String(nullable: false, maxLength: 20),
                        invTransactionDate = c.DateTime(),
                        invLastUpdate = c.DateTime(),
                    })
                .PrimaryKey(t => t.invID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Invoices");
        }
    }
}
