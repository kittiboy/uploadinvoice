﻿using System;

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.IO;
using System.Web;
using System.Web.Mvc;
using UploadApp.Models;
using CsvHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Xml;
using System.Net.Http;

namespace UploadApp.Controllers
{
    public class InvoiceController : Controller
    {
        private UploadDBContextcs db = new UploadDBContextcs();

        // GET: Invoice
        public ActionResult Index()
        {
            return View(db.Invoices.ToList());
        }
        public ActionResult Upload()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase postedFile)
        {
            bool IsSuccessful = true;
            bool IsError = false;
            if (postedFile != null)
            {
                try
                {
                    string fileExtension = Path.GetExtension(postedFile.FileName);
                    if (fileExtension.ToLower() == ".xml" && postedFile.ContentLength > 0)
                    {
                        int rowId = 0;
                        decimal amt = 0;
                        DateTime invDate = new DateTime(1970, 1, 1);
                        DateTime utcDate = new DateTime(1970, 1, 1);
                        var document = new XmlDocument();
                        document.Load(postedFile.InputStream);
                        XmlNodeList xnList = document.SelectNodes("/Transactions/Transaction");
                        foreach (XmlNode xn in xnList)
                        {
                            rowId++;
                            fileInvoice invoice = new fileInvoice();
                            if (xn.Name == "Transaction")
                            {
                                invoice.invCode = xn.Attributes["id"].Value;
                                invoice.invTransactionDate = xn["TransactionDate"].InnerText;
                                invoice.invStatus = xn["Status"].InnerText;
                                XmlNode Payment = xn.SelectSingleNode("PaymentDetails");
                                if (Payment != null)
                                {
                                    invoice.invAmount = Payment["Amount"].InnerText;
                                    invoice.invCurrencyCode = Payment["CurrencyCode"].InnerText;
                                    
                                }
                            }

                            if (invoice.invCode.Length == 0)
                            {
                                SaveInvoiceError(postedFile.FileName, "Code is empty", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invAmount.Length == 0 || !Decimal.TryParse(invoice.invAmount.ToString(), out amt))
                            {
                                SaveInvoiceError(postedFile.FileName, "Invalid amount", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invCurrencyCode.Length == 0)
                            {
                                SaveInvoiceError(postedFile.FileName, "Currency code is empty", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invStatus.Length == 0)
                            {
                                SaveInvoiceError(postedFile.FileName, "Status is empty", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invTransactionDate.Length == 0 || !DateTime.TryParseExact(invoice.invTransactionDate, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out invDate))
                            {
                                SaveInvoiceError(postedFile.FileName, "Invalid transaction date", "RowId: " + rowId.ToString());
                            }
                            else
                            {
                                SaveInvoiceData(postedFile.FileName, rowId, invoice.invCode, amt, invoice.invCurrencyCode, invoice.invStatus, invDate).ToString();
                                ViewBag.Message = "Upload xml successful ";
                            }

                        }

                       // ViewBag.Message = ViewBag.Message + invDate.ToString("yyyy-MM-dd HH:mm:ss");
                        IsSuccessful = true;
                    }
                    else if (fileExtension.ToLower() == ".csv" && postedFile.ContentLength > 0)
                    {
                        ICsvParser csvParser = new CsvParser(new StreamReader(postedFile.InputStream));
                        using (var csv = new CsvReader(csvParser))
                        {
                            csv.Configuration.HasHeaderRecord = false;
                            var records = csv.GetRecords<fileInvoice>();
                            int rowId = 0;
                            decimal amt = 0;
                            DateTime invDate = new DateTime(1970, 1, 1);
                            foreach (fileInvoice row in records)
                            {
                                rowId++;
                                if (row.invCode.Length == 0) {
                                    SaveInvoiceError(postedFile.FileName, "Code is empty", "RowId: " + rowId.ToString());
                                }
                                else if (row.invAmount.Length == 0 || !Decimal.TryParse(row.invAmount.ToString(), out amt))
                                {
                                    SaveInvoiceError(postedFile.FileName, "Invalid amount", "RowId: " + rowId.ToString());
                                }
                                else if (row.invCurrencyCode.Length == 0)
                                {
                                    SaveInvoiceError(postedFile.FileName, "Currency code is empty", "RowId: " + rowId.ToString());
                                }
                                else if (row.invStatus.Length == 0)
                                {
                                    SaveInvoiceError(postedFile.FileName, "Status is empty", "RowId: " + rowId.ToString());
                                }
                                else if (row.invTransactionDate.Length == 0 || !DateTime.TryParseExact(row.invTransactionDate,"dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None,out invDate))
                                {
                                    SaveInvoiceError(postedFile.FileName, "Invalid transaction date", "RowId: " + rowId.ToString());
                                }
                                else {
                                    SaveInvoiceData(postedFile.FileName, rowId, row.invCode, amt, row.invCurrencyCode, row.invStatus, invDate).ToString();
                                }
                            }
                            ViewBag.Message = "Upload csv successful";

                            IsSuccessful = true;
                        }
                    }
                    else {
                        ViewBag.Message = "Please select .csv or .xml file extension";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            else
            {
                ViewBag.Message = "Please select the file first to upload.";
            }

           // return Json(new { message = ViewBag.Message });
            
            if (IsSuccessful) {
                return RedirectToAction("Index", "Invoice");
            } else {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UploadInvoice()
        {
            HttpPostedFileBase postedFile = Request.Files["file"];
            bool IsSuccessful = true;
            if (postedFile != null)
            {
                try
                {
                    string fileExtension = Path.GetExtension(postedFile.FileName);
                    if (fileExtension.ToLower() == ".xml" && postedFile.ContentLength > 0)
                    {
                        int rowId = 0;
                        decimal amt = 0;
                        DateTime invDate = new DateTime(1970, 1, 1);
                        DateTime utcDate = new DateTime(1970, 1, 1);
                        var document = new XmlDocument();
                        document.Load(postedFile.InputStream);
                        XmlNodeList xnList = document.SelectNodes("/Transactions/Transaction");
                        foreach (XmlNode xn in xnList)
                        {
                            rowId++;
                            fileInvoice invoice = new fileInvoice();
                            if (xn.Name == "Transaction")
                            {
                                invoice.invCode = xn.Attributes["id"].Value;
                                invoice.invTransactionDate = xn["TransactionDate"].InnerText;
                                invoice.invStatus = xn["Status"].InnerText;
                                XmlNode Payment = xn.SelectSingleNode("PaymentDetails");
                                if (Payment != null)
                                {
                                    invoice.invAmount = Payment["Amount"].InnerText;
                                    invoice.invCurrencyCode = Payment["CurrencyCode"].InnerText;

                                }
                            }

                            if (invoice.invCode.Length == 0)
                            {
                                SaveInvoiceError(postedFile.FileName, "Code is empty", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invAmount.Length == 0 || !Decimal.TryParse(invoice.invAmount.ToString(), out amt))
                            {
                                SaveInvoiceError(postedFile.FileName, "Invalid amount", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invCurrencyCode.Length == 0)
                            {
                                SaveInvoiceError(postedFile.FileName, "Currency code is empty", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invStatus.Length == 0)
                            {
                                SaveInvoiceError(postedFile.FileName, "Status is empty", "RowId: " + rowId.ToString());
                            }
                            else if (invoice.invTransactionDate.Length == 0 || !DateTime.TryParseExact(invoice.invTransactionDate, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out invDate))
                            {
                                SaveInvoiceError(postedFile.FileName, "Invalid transaction date", "RowId: " + rowId.ToString());
                            }
                            else
                            {
                                SaveInvoiceData(postedFile.FileName, rowId, invoice.invCode, amt, invoice.invCurrencyCode, invoice.invStatus, invDate).ToString();
                                ViewBag.Message = "Upload xml successful ";
                            }

                        }

                        // ViewBag.Message = ViewBag.Message + invDate.ToString("yyyy-MM-dd HH:mm:ss");
                        IsSuccessful = true;
                    }
                    else if (fileExtension.ToLower() == ".csv" && postedFile.ContentLength > 0)
                    {
                        ICsvParser csvParser = new CsvParser(new StreamReader(postedFile.InputStream));
                        using (var csv = new CsvReader(csvParser))
                        {
                            csv.Configuration.HasHeaderRecord = false;
                            var records = csv.GetRecords<fileInvoice>();
                            int rowId = 0;
                            decimal amt = 0;
                            DateTime invDate = new DateTime(1970, 1, 1);
                            foreach (fileInvoice row in records)
                            {
                                rowId++;
                                if (row.invCode.Length == 0)
                                {
                                    SaveInvoiceError(postedFile.FileName, "Code is empty", "RowId: " + rowId.ToString());
                                }
                                else if (row.invAmount.Length == 0 || !Decimal.TryParse(row.invAmount.ToString(), out amt))
                                {
                                    SaveInvoiceError(postedFile.FileName, "Invalid amount", "RowId: " + rowId.ToString());
                                }
                                else if (row.invCurrencyCode.Length == 0)
                                {
                                    SaveInvoiceError(postedFile.FileName, "Currency code is empty", "RowId: " + rowId.ToString());
                                }
                                else if (row.invStatus.Length == 0)
                                {
                                    SaveInvoiceError(postedFile.FileName, "Status is empty", "RowId: " + rowId.ToString());
                                }
                                else if (row.invTransactionDate.Length == 0 || !DateTime.TryParseExact(row.invTransactionDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out invDate))
                                {
                                    SaveInvoiceError(postedFile.FileName, "Invalid transaction date", "RowId: " + rowId.ToString());
                                }
                                else
                                {
                                    SaveInvoiceData(postedFile.FileName, rowId, row.invCode, amt, row.invCurrencyCode, row.invStatus, invDate).ToString();
                                }
                            }
                            ViewBag.Message = "Upload csv successful";

                            IsSuccessful = true;
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Please select .csv or .xml file extension";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            else
            {
                ViewBag.Message = "Please select the file first to upload.";
            }
            // return Json(new { message = ViewBag.Message });
           
            if (IsSuccessful)
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest); 
            }
        }

        public JsonResult getInvoiceByCurrency(string CurrencyCode)
        {
            List<userInvoice> usrInvoice = new List<userInvoice>();
            using (UploadDBContextcs db = new UploadDBContextcs())
            {
                string strSQL = "SELECT invCode as id,CAST(invAmount AS varchar(20))+ ' '+ invCurrencyCode as payment,Case When invStatus= 'Failed' OR invStatus = 'Rejected' Then 'R' When invStatus= 'Finished' OR invStatus = 'Done' Then 'D' ELSE 'A' END as [Status] " +
                " FROM Invoices WHERE invCurrencyCode=@CurrencyCode";
                usrInvoice = db.userInvoices.SqlQuery(strSQL, new SqlParameter("@CurrencyCode", CurrencyCode)).ToList();
            }
            return Json(usrInvoice, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getInvoiceByStatus(string Status)
        {
            List<userInvoice> usrInvoice = new List<userInvoice>();
            using (UploadDBContextcs db = new UploadDBContextcs())
            {
                string strSQL = "SELECT invCode as id,CAST(invAmount AS varchar(20))+ ' '+ invCurrencyCode as payment,Case When invStatus= 'Failed' OR invStatus = 'Rejected' Then 'R' When invStatus= 'Finished' OR invStatus = 'Done' Then 'D' ELSE 'A' END as [Status] " +
                " FROM Invoices WHERE (Case When invStatus= 'Failed' OR invStatus = 'Rejected' Then 'R' When invStatus= 'Finished' OR invStatus = 'Done' Then 'D'  When  invStatus='Approved' Then 'A' END =  @Status) OR (invStatus =  @Status)";
                usrInvoice = db.userInvoices.SqlQuery(strSQL, new SqlParameter("@Status", Status)).ToList();
            }
            return Json(usrInvoice, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getInvoiceByDate(DateTime SDate,DateTime EDate)
        {
            string strSDate = SDate.ToString("yyyy-MM-dd");
            string strEDate = EDate.ToString("yyyy-MM-dd");
            List<userInvoice> usrInvoice = new List<userInvoice>();
            using (UploadDBContextcs db = new UploadDBContextcs())
            {
                string strSQL = "SELECT invCode as id,CAST(invAmount AS varchar(20))+ ' '+ invCurrencyCode as payment,Case When invStatus= 'Failed' OR invStatus = 'Rejected' Then 'R' When invStatus= 'Finished' OR invStatus = 'Done' Then 'D' ELSE 'A' END as [Status] " +
                " FROM Invoices WHERE Convert(DATE,invTransactionDate) Between  Convert(DATE,@SDate,120) AND Convert(DATE,@EDate,120) ";
                usrInvoice = db.userInvoices.SqlQuery(strSQL, new SqlParameter("@SDate", strSDate), new SqlParameter("@EDate", strEDate)).ToList();
            }
            return Json(usrInvoice, JsonRequestBehavior.AllowGet);
        }

        private bool SaveInvoiceData(string fileName,int RowId,string invCode,decimal invAmount,string invCurrencyCode,string invStatus,DateTime invTransactionDate) {
            bool IsSuccessful = false;
            try
            {
                string ConnString = ConfigurationManager.ConnectionStrings["ConnUploadDBContext"].ConnectionString;
                SqlConnection conn = new SqlConnection(ConnString); 
                using (SqlCommand cmd = new SqlCommand())
                {
                    string strSQL = "spSaveInvoiceData";
                    cmd.Connection = conn;
                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@fileName", SqlDbType.NVarChar).Value = fileName;
                    cmd.Parameters.Add("@RowId", SqlDbType.Int).Value = RowId;
                    cmd.Parameters.Add("@invCode", SqlDbType.NVarChar).Value = invCode;
                    cmd.Parameters.Add("@invAmount", SqlDbType.Decimal).Value = invAmount;
                    cmd.Parameters.Add("@invCurrencyCode", SqlDbType.NVarChar).Value = invCurrencyCode;
                    cmd.Parameters.Add("@invStatus", SqlDbType.NVarChar).Value = invStatus;
                    cmd.Parameters.Add("@invTransactionDate", SqlDbType.DateTime).Value = invTransactionDate;
                    cmd.Parameters.Add("@updStatus", SqlDbType.Bit).Direction = ParameterDirection.Output;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    bool updStatus = Convert.ToBoolean(cmd.Parameters["@updStatus"].Value.ToString());
                    conn.Close();
                    conn.Dispose();
                    IsSuccessful = updStatus;
                }                    
            }
            catch (Exception ex) {
              //  IsSuccessful = ex.Message;
            }
            return IsSuccessful;
        }

        private int SaveInvoiceError(string exErrName, string exErrMsg, string exRefData)
        {
            int OutID = 0;
            try
            {
                string ConnString = ConfigurationManager.ConnectionStrings["ConnUploadDBContext"].ConnectionString;
                SqlConnection conn = new SqlConnection(ConnString);
                using (SqlCommand cmd = new SqlCommand())
                {
                    string strSQL = "spSaveInvoiceError";
                    cmd.Connection = conn;
                    cmd.CommandText = strSQL;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@exErrName", SqlDbType.NVarChar).Value = exErrName;
                    cmd.Parameters.Add("@exErrMsg", SqlDbType.NVarChar).Value = exErrMsg;
                    cmd.Parameters.Add("@exRefData", SqlDbType.NVarChar).Value = exRefData;
                    cmd.Parameters.Add("@OutID", SqlDbType.Int).Direction = ParameterDirection.Output; 
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    OutID = Convert.ToInt32(cmd.Parameters["@OutID"].Value.ToString());
                    conn.Close();
                    conn.Dispose();
                }
            }
            catch (Exception ex)
            {
                //  IsSuccessful = ex.Message;
            }
            return OutID;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
