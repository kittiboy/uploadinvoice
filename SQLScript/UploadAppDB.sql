USE [InvoiceDB]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/30/2020 2:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exception_Log]    Script Date: 10/30/2020 2:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exception_Log](
	[exID] [int] IDENTITY(1,1) NOT NULL,
	[exErrName] [nvarchar](100) NOT NULL,
	[exErrMsg] [nvarchar](250) NULL,
	[exRefData] [nvarchar](50) NULL,
	[exCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Exception_Log] PRIMARY KEY CLUSTERED 
(
	[exID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 10/30/2020 2:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[invID] [int] IDENTITY(1,1) NOT NULL,
	[invCode] [nvarchar](50) NOT NULL,
	[invAmount] [decimal](18, 2) NOT NULL,
	[invCurrencyCode] [nvarchar](3) NOT NULL,
	[invStatus] [nvarchar](20) NOT NULL,
	[invTransactionDate] [datetime] NULL,
	[invLastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.Invoices] PRIMARY KEY CLUSTERED 
(
	[invID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Exception_Log] ADD  CONSTRAINT [DF_Exception_Log_exCreated]  DEFAULT (getdate()) FOR [exCreated]
GO
ALTER TABLE [dbo].[Invoices] ADD  CONSTRAINT [DF_Invoices_invLastUpdate]  DEFAULT (getdate()) FOR [invLastUpdate]
GO
/****** Object:  StoredProcedure [dbo].[spSaveInvoiceData]    Script Date: 10/30/2020 2:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveInvoiceData](@fileName nvarchar(100),@RowId int,@invCode nvarchar(50),@invAmount decimal(18,2),@invCurrencyCode nvarchar(3),@invStatus nvarchar(20),@invTransactionDate DateTime,@updStatus bit Output)
AS
BEGIN TRY
	SET NOCOUNT ON;
	MERGE INTO [dbo].[Invoices] Dst
		USING (
			SELECT @invCode as invCode,@invAmount as invAmount,@invCurrencyCode as invCurrencyCode ,@invStatus as invStatus,@invTransactionDate as invTransactionDate
		) as Src on  Dst.invCode=Src.invCode  
		WHEN NOT MATCHED BY TARGET THEN 
				INSERT (invCode,invAmount,invCurrencyCode,invStatus,invTransactionDate)
				VALUES (Src.invCode,Src.invAmount,Src.invCurrencyCode,Src.invStatus,Src.invTransactionDate)
		WHEN MATCHED THEN 
			UPDATE SET invCode = Src.invCode , invAmount = Src.invAmount,invCurrencyCode = Src.invCurrencyCode,invStatus=Src.invStatus ,invTransactionDate=Src.invTransactionDate ,invLastUpdate=GETDATE()
		;
		SET @updStatus = 1
END TRY
BEGIN CATCH
		SET @updStatus = 0
		INSERT INTO Exception_Log (exErrName,exErrMsg,exRefData) VALUES (OBJECT_NAME(@@PROCID) , ' Database error: [' + CONVERT(varchar(200), ERROR_NUMBER()) + '] [' + ERROR_MESSAGE() + ']',@fileName+ ' [RowId :'+ CAST(@RowID as varchar(10))+']')
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[spSaveInvoiceError]    Script Date: 10/30/2020 2:06:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSaveInvoiceError](@exErrName nvarchar(100),@exErrMsg nvarchar(250),@exRefData nvarchar(50),@OutID int Output )
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
		INSERT INTO [Exception_Log] (exErrName,exErrMsg,exRefData) VALUES (@exErrName , @exErrMsg,@exRefData)
		SET @OutID = SCOPE_IDENTITY()
	END TRY
	BEGIN CATCH
		INSERT INTO [Exception_Log] (exErrName,exErrMsg) VALUES (OBJECT_NAME(@@PROCID) , ' Database error: [' + CONVERT(varchar(200), ERROR_NUMBER()) + '] [' + ERROR_MESSAGE() + ']')
		SET @OutID = 0
END CATCH
END
GO
