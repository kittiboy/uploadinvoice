﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UploadApp.Models
{
    public class Exception_Log
    {
        [Key]
        public int exID { get; set; }
        [Required]
        [StringLength(100)]
        public string exErrName { get; set; }
        [StringLength(250)]
        public string exErrMsg { get; set; }
        [StringLength(50)]
        public string exRefData { get; set; }
        public DateTime? exCreated { get; set; }
    }
}