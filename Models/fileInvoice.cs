﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UploadApp.Models
{
    public class fileInvoice
    {
        public string invCode { get; set; }
        public string invAmount { get; set; }
        public string invCurrencyCode { get; set; }
        public string invTransactionDate { get; set; }
        public string invStatus { get; set; }
    }
}