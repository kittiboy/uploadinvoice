﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace UploadApp.Models
{
    public class UploadDBContextcs : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public UploadDBContextcs() : base("name=ConnUploadDBContext")
        {
        }

        public System.Data.Entity.DbSet<UploadApp.Models.Invoice> Invoices { get; set; }
        public System.Data.Entity.DbSet<UploadApp.Models.userInvoice> userInvoices { get; set; }
        public System.Data.Entity.DbSet<UploadApp.Models.Exception_Log> Exception_Logs { get; set; }
    }
}