﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace UploadApp.Models
{
    public class Invoice
    {
        [Key]
        public int invID { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Invoice")]
        public string invCode { get; set; }
        [Required]
        [Display(Name = "Amount")]
        public decimal invAmount { get; set; }
        [Required]
        [StringLength(3)]
        [Display(Name = "Currency Code")]
        public string invCurrencyCode { get; set; }
        [Required]
        [StringLength(20)]
        [Display(Name = "Status")]
        public string invStatus { get; set; }
        [Display(Name = "Transaction Date")]
        public DateTime? invTransactionDate { get; set; }
        [Display(Name = "Last Update")]
        public DateTime? invLastUpdate { get; set; }
    }
}